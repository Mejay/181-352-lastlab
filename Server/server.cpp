#include "server.h"

Server::Server(QObject *pwgt) : QObject(pwgt), m_nNextBlockSize(0)
{
    tcpServer = new QTcpServer(this);
}

QList<QTcpSocket *> Server::getClients()
{
    return clients;
}
QByteArray request; // Сырые данные для запроса и ответа


void Server::newConnection()
{
    QTcpSocket *clientSocket = tcpServer->nextPendingConnection();

    connect(clientSocket, &QTcpSocket::disconnected, clientSocket, &QTcpSocket::deleteLater);
    connect(clientSocket, &QTcpSocket::readyRead, this, &Server::readClient);
    connect(clientSocket, &QTcpSocket::disconnected, this, &Server::gotDisconnection);

    clients << clientSocket;// сохраняем сокет клиента

    sendToClient(clientSocket, "Reply: connection established");
}






QJsonDocument docRequest = QJsonDocument::fromJson(request); // Получаем содержимое запроса
QJsonObject obj = docRequest.object(); // Получаем документ в виде объекта



QString inputLogin = obj.find("data")->toObject().find("login")->toString(); // Считываем введенный логин
QString inputPass = obj.find("data")->toObject().find("pass")->toString(); // Считаем введеный пароль



using namespace std;

void Server::readClient()
{
    QTcpSocket *clientSocket = (QTcpSocket*)sender();

    QDataStream in(clientSocket);
    //in.setVersion(QDataStream::Qt_5_10);
    for (;;)
    {


        if (!m_nNextBlockSize) {
                if (clientSocket->bytesAvailable() < sizeof(quint16)) { break; }
            in >> m_nNextBlockSize;
        }

        if (clientSocket->bytesAvailable() < m_nNextBlockSize) { break; }
        QString connect_request;
        in >> connect_request;
        qDebug() << "CRIPT: " + connect_request; // считываем последовательно статус и запрос




        if(connect_request != "STATUS_ADD"){ // проверка на статус и запрос
            if(statusLogIn != "LOG_IN"){
                 statusLogIn = connect_request;
                 qDebug() << "STATUS: " + statusLogIn;
            }
        }

            if(statusLogIn == "LOG_IN" && connect_request != "LOG_IN"){
                QSqlQuery query(QSqlDatabase::database("MyConnect"));
                query.prepare(QString(connect_request));

            if(!query.exec()){
                qDebug() << "Failad: Query failed to Execute";
            }else {
                    statusLogIn = "";
                while(query.next()){
                    log = connect_request;
                    QString usernameFromDB = query.value(1).toString();
                    QString passwordFromDB = query.value(2).toString();
                    QString status = query.value(5).toString();

                    if(status == "admin")
                    {
                        QByteArray admin = "LOG_IN_ADMIN";
                        qDebug() << "LOG_IN_ADMIN";
                         sendToClient(clientSocket, admin);
                    }

                    if(status == "user")
                    {
                        QByteArray user = "LOG_IN_USER";
                        qDebug() << "LOG_IN_USER";
                         sendToClient(clientSocket, user);
                    }

                    emit gotNewMesssage("Request: " + connect_request);
                    emit gotNewMesssage("Success: Attempt to enter...\n");

                    qDebug() << "Success: Attempt to enter...";


                }

            }
        }



            if(statusAdd != "STATUS_ADD" && connect_request != "LOG_IN" && connect_request != log){
                 statusAdd = connect_request;
                 qDebug() << "STATUS add: " + statusAdd;
            }

                if(statusAdd == "STATUS_ADD" && connect_request != "STATUS_ADD"){
                    QSqlQuery query(QSqlDatabase::database("MyConnect"));
                    query.prepare(QString(connect_request));

                    if(!query.exec()){
                            statusAdd = "";
                            add = connect_request;

                            emit gotNewMesssage("Success: Success Add\n");
                            qDebug() << "Success: Success Add";


                            QByteArray add = "STATUS_ADD";
                            sendToClient(clientSocket, add);
                        }

                }

                if(statusDelete != "STATUS_DELETE" && connect_request != "LOG_IN" && connect_request != log){
                     statusDelete = connect_request;
                     qDebug() << "STATUS delete: " + statusDelete;
                }

                    if(statusDelete == "STATUS_DELETE" && connect_request != "STATUS_DELETE"){
                        QSqlQuery query(QSqlDatabase::database("MyConnect"));
                        query.prepare(QString(connect_request));

                        if(query.exec()){

                                statusDelete = "";


                                emit gotNewMesssage("Success: Success Delete\n");

                                QByteArray deleted = "STATUS_DELETE";

                                sendToClient(clientSocket, deleted);

                            }

                    }


                    if(changeTable != "CHANGE"){
                         changeTable = connect_request;
                         qDebug() << "STATUS CHANGE: " + changeTable;
                    }

                    if(changeTable == "CHANGE" && connect_request != "CHANGE")
                    {
                        QSqlQuery query(QSqlDatabase::database("MyConnect"));
                        query.prepare(QString(connect_request));

                        if(!query.exec()){
                            changeTable = "";
                            QString ch = "CHANGE";
                            sendToClient(clientSocket, ch);


                            emit gotNewMesssage("Success: Success Change\n");
                        }
                    }


                    if(showTable != "SHOW_TABLE"){
                         showTable = connect_request;
                         qDebug() << "STATUS SHOW_TABLE: " + showTable;
                    }

                    if(showTable == "SHOW_TABLE" && connect_request != "SHOW_TABLE")
                    {
                        showTable = "";

                        QString basa = "";
                        QSqlQuery query(QSqlDatabase::database("MyConnect"));
                        query.exec(connect_request);

                        while (query.next())
                        {
                            basa += "\n--------------------------------      ID:" + query.value(0).toString() + "\n      Username:" + query.value(1).toString() + "\n      Password:" +
                                    query.value(2).toString() + "\n      Email:" + query.value(3).toString()+ "\n      Phone:" +
                                    query.value(4).toString() +  "\n      Status:" + query.value(5).toString();
                        }




                        QByteArray show = "SHOW_TABLE";
                        sendToClient(clientSocket, show);
                        sendToClient(clientSocket, basa);


                        showTable = "";

                        emit gotNewMesssage("Success: Show Table\n");

                    }


                    if(searchTable != "SEARCH"){
                         searchTable = connect_request;

                    }

                    if(searchTable == "SEARCH" && connect_request != "SEARCH")
                    {
                        searchTable = "";

                        QString basa = "";
                        QSqlQuery query(QSqlDatabase::database("MyConnect"));
                        query.exec(connect_request);

                        while (query.next())
                        {
                            basa += "\n--------------------------------      ID:" + query.value(0).toString() + "\n      Username:" + query.value(1).toString() + "\n      Password:" +
                                    query.value(2).toString() + "\n      Email:" + query.value(3).toString()+ "\n      Phone:" +
                                    query.value(4).toString() +  "\n      Status:" + query.value(5).toString();
                        }



                        QByteArray show = "SEARCH";
                        sendToClient(clientSocket, show);
                        sendToClient(clientSocket, basa);

                        searchTable = "";
                        emit gotNewMesssage("Success: Success Search\n");
                    }











        m_nNextBlockSize = 0;


    }
}

void Server::gotDisconnection()
{
    clients.removeAt(clients.indexOf((QTcpSocket*)sender()));
    emit smbDisconnected();
}

qint64 Server::sendToClient(QTcpSocket* socket, const QString& str)
{
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);

    out << quint16(0) << str;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

    return socket->write(arrBlock);
}


