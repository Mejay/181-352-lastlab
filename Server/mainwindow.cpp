#include "mainwindow.h"
#include "ui_mainwindow.h"
QString message;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    server = new Server(this);
    connect(server, &Server::gotNewMesssage,
            this, &MainWindow::gotNewMesssage);
    connect(server->tcpServer, &QTcpServer::newConnection,
            this, &MainWindow::smbConnectedToServer);
    connect(server, &Server::smbDisconnected,
            this, &MainWindow::smbDisconnectedFromServer);

    if (!server->tcpServer->listen(QHostAddress::Any, 6547))
    {
        ui->textEdit_log->append(tr("Error!The port is taken by some other service."));
        return;
    }
    connect(server->tcpServer, &QTcpServer::newConnection, server, &Server::newConnection);
    ui->textEdit_log->append(tr("Server started."));
}

MainWindow::~MainWindow()
{
    delete server;
    delete ui;
}



void MainWindow::smbConnectedToServer()
{
    ui->textEdit_log->append(tr("Somebody has connected"));
}

void MainWindow::smbDisconnectedFromServer()
{
    ui->textEdit_log->append(tr("Somebody has disconnected"));
}

void MainWindow::gotNewMesssage(QString connect_request)
{
    ui->textEdit_log->append(connect_request);
}
