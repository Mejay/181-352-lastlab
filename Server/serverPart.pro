QT += core gui network widgets sql

TEMPLATE = app
TARGET = server

MOC_DIR     += generated/mocs
UI_DIR      += generated/uis
RCC_DIR     += generated/rccs
OBJECTS_DIR += generated/objs

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    server.cpp \
    qaesencryption.cpp

HEADERS += \
    mainwindow.h \
    server.h \
    qaesencryption.h

FORMS += \
    mainwindow.ui


